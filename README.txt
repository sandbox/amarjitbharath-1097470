CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Uninstalling

INTRODUCTION
------------

Authors:
* Amarjit Bharath (Amarjit)

This Module was made by Amarjit Bharath (amarjit@bharath.co.uk)


The recently viewed nodes module allows you to log any nodes
a user visits. This works for both anonymous users and
logged in users. You can limit what nodes are logged by type
and to specific roles.

The list can also be saved to database, with flexible admin
settings that will allow you to tailor the module to your
server.

The list is saved with nid, nide type, title and path.
It is now possible to also force a re-check on if nodes exist.
You can also track unpublished nodes. This is useful for when
you have nodes in views that you do not want to appear in search.

An example use of the module is for UberCart. You can log
anonymous nodes visits and when the user logs in, it
will automatically save the list for that user.

You may also use the public function recently_viewed_nodes_session_manage('view')
to get an array'd list of nodes visited. This is useful for
custom coding.

INSTALLATION
------------

1. Copy the recently_viewed_nodes module directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules.

3. Set your settings via the settings page in the administration section.

4. Enable the block.

5. (Optional) Copy the .tpl (template file) to your theme to override the default themed block.


UNINSTALLING
------------

1. Disable the module via Administer >> Site building >> Modules.

2. (Optional) Delete the recently_viewed_nodes from your module folder.